import React from "react";

const SearchResult = props => {
  const { name } = props.user;
  return (
    <div className="seach-result-user-card">
      <div
        className="search-user-name"
        onClick={() => props.addMemberToGroup()}
      >
        {name}
      </div>
    </div>
  );
};

export default SearchResult;
