import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { logoutUser } from "../../store/actions";
import { Link } from "react-router-dom";
import * as ROUTES from "../../constants/routes";
import ymlLogo from "../../assets/icn-logo3x.png";
import { connect } from "react-redux";
import "./profileSidebar.css";

class ProfileSidebar extends Component {
  logout = () => {
    this.props.logoutUser(this.props.history);
  };
  render() {
    const user = this.props.user;
    return (
      <div className="profile-sidebar-container">
        <Link to={ROUTES.HOME}>
          <img src={ymlLogo} className="sidebar-yml-logo" alt="YML-logo" />
        </Link>
        <div className="sidebar-profile-image-container">
          <img
            className="sidebar-profile-image"
            src={user.avatarUrl}
            alt="user-profile"
          />
        </div>
        <div className="sidebar-profile-name">{user.name}</div>
        <div className="sidebar-profile-email">{user.emailId}</div>
        <button
          className="profile-sidebar-logout"
          type="button"
          onClick={this.logout}
        >
          <i className="fa fa-sign-out" aria-hidden="true"></i>
        </button>
      </div>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    logoutUser: history => dispatch(logoutUser(history))
  };
};
export default connect(null, mapDispatchToProps)(withRouter(ProfileSidebar));
