import React from "react";
import MemberItem from "./MemberItem";
import "./member.css";

const MemberList = ({ items, removeItem }) => {
  return (
    <div className="members-list">
      {items.map((item, index) => (
        <MemberItem
          key={Math.random()
            .toString(36)
            .substring(2, 15)}
          text={item}
          itemIndex={index}
          removeItem={removeItem}
        />
      ))}
    </div>
  );
};
export default MemberList;
